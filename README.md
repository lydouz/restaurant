README.MD

Le projet représente une application qui permet de visualiser les repas ou les menus du restaurant
avec tous les détails et les prix, choisir son menu en ligne puis ensuite partir au restaurant. Elle se
compose de trois pages : Accueil, Menu et Contact.

**La page « Accueil »**


*  La page d’accueil contient une image de l’intérieur du restaurant permettant au visiteur de
l’application d’avoir une idée sur le restaurant avant d’y aller.

*  La phrase « bienvenu dans notre restaurant » est une phrase d’accueil et au même temps
elle représente un lien vers le site web officiel du restaurant permettant de le visiter.

*   Elle permet également aux utilisateurs de noter l’application ce qui va permettre au
restaurant d’améliorer et de prendre en compte les besoins de ses clients.


*  A partir de cette page, l’utilisateur peut directement passer à la page contact pour contacter
le restaurant à travers le bouton « voir la page contact ».

*  Le menu « catégorie » donne la possibilité de choisir (en sélectionnant) entre « entrée, plat
ou dessert » à afficher selon le souhait du client.

*  A partir de cette page, l’utilisateur peut accéder à la page « Menu » et « Contact » .

**La page « Menu »**


*  Elle contient les plats du jour, représentés sous forme d’une liste avec des images
montrant les plats et leurs prix en euro.

*  A partir de cette page, l’utilisateur peut accéder à la page « Accueil » et « Contact » .

**La page « Contact »**


*  Représente l’interface entre le restaurant et ses clients. Une fois le client a choisi son
plat ou son menu dans le page Menu, la page contacte lui permettra de passer sa
commande par mail.

*  Cette page peut aussi être utilisée pour exprimer d’autres besoins d’où le champ
« Sujet »

*  Pour rester en contact avec ses clients, le restaurant mets à leurs dispositions ses
cordonnées pour y accéder directement.

*  A partir de cette page, l’utilisateur peut accéder à la page « Accueil » et « Menu ».